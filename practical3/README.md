# Practical 2: Learning Word Representations

This repository contains the code for our third practical for the Unsupervised Language Learning course. In this practical, we evaluated Skipgram embeddings as well as precomputed EmbedAlign embeddings.

## Installation & Dependencies
After downloading or cloning our repository locally, the following dependencies should be satisfied before running our code.

### Environment
Our code is written in a [Jupyter notebook](http://jupyter.org/install) which we ran using a Python 3.5 kernel.

### Libraries
We use the following non-default libraries, which should be installed before running our code:

* [gensim](https://radimrehurek.com/gensim/)
* [numpy](http://www.numpy.org/)
* [tensorflow](https://www.tensorflow.org/)
* [matplotlib](https://matplotlib.org/)

### SentEval datasets
To obtain the SentEval datasets for evaluation, the `get_transfer_data.bash` script in the `SentEval/data/downstream` directory should be run.

## Models
The code for our model and evaluations are found in Jupyter notebooks with the corresponding names:

* `gensim_skipgram.ipynb` contains code for training a Skipgram model using gensim. It also creates the `vocab.npy` file that contains the document frequencies for each item in the vocab.
* `senteval_skipgram_unweighted.ipynb` is almost the exact same file as the one that was provided for evaluating word embeddings with SentEval. The only changes are pointers to the right file locations.
* `senteval_skipgram_tfidf.ipynb` is almost the same as the the unweighted version, except with a few changes to the batcher function, so that it uses a TF-IDF weighting scheme instead of simply taking the unweighted mean.
* `senteval_embedalign.ipynb` is the same file provided to us with minor changes to point to the right directories.

## Usage
Previous results from our own runs of the evaluation tasks are shown when opening the notebooks, and the code can also be run from scratch. The notebook content can also be observed on Github directly.

## Credits
This practical was made by [Rasyan Ahmed](https://github.com/rasyan) & [Marianne de Heer Kloots](https://github.com/mdhk).
