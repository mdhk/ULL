# /results/
In this folder we store some pickled results (the accuracies, reciprocal ranks and predictions from the analogy task), to save time:
* `accuracies.pkl`
* `predictions.pkl`
* `reciprocal_ranks.pkl`
