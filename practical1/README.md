# Practical 1: Evaluating Word Representations

This repository contains the code for our first practical for the Unsupervised Language Learning course. In this practical, we qualitatively and quantitively analyzed the properties of three precomputed word embedding models. The main file to run to obtain our results is `main.ipynb`. This file can also be observed on github directly.

## Installation & Dependencies
After downloading or cloning our repository locally, a few things should be put in place before running our code. In particular we use a few libraries that should be installed and need a few datasets that should be downloaded and placed in their respective folders. This is explained below.

### Environment
Our code is written in a [Jupyter notebook](http://jupyter.org/install) which we ran using a Python 3.5 kernel.

### Libraries
We use the following libraries, which should be installed before running our code:

* [gensim](https://radimrehurek.com/gensim/)
* [pandas](https://pandas.pydata.org/)
* [numpy](http://www.numpy.org/)
* [pickle](https://docs.python.org/3/library/pickle.html)
* [sklearn](http://scikit-learn.org/stable/)
* [matplotlib](https://matplotlib.org/index.html)

### Datasets
We use the [SimLex](https://www.cl.cam.ac.uk/~fh295/simlex.html) and [MEN](https://staff.fnwi.uva.nl/e.bruni/MEN) datasets for the similarity task and the [Google analogy](https://aclweb.org/aclwiki/Google_analogy_test_set_(State_of_the_art)) dataset for the analogy task. We also used [a list of 2000 nouns](https://uva-slpl.github.io/ull/resources/practicals/practical1/2000_nouns_sorted.txt) that was provided with the assignment for the clustering task. These files should be obtained from the respective sources and put into the following folders before running our code:

* `SimLex-999.txt` into the `/datasets/SimLex-999/` folder
* `MEN_dataset_natural_form_full` into the `/datasets/MEN/` folder
* `questions-words.txt` into the `/datasets/Google_analogy/` folder
* `2000_nouns_sorted.txt` into the `/words/2000nouns/` folder

### Embeddings
We obtained the [Levy & Goldberg (2014)](http://www.aclweb.org/anthology/P14-2050) embeddings from [https://levyomer.wordpress.com/2014/04/25/dependency-based-word-embeddings/](https://levyomer.wordpress.com/2014/04/25/dependency-based-word-embeddings/), and store the following files into the `/embeddings/` folder:

* `bow2.words`
* `bow5.words`
* `deps.words`

We load these with gensim as KeyedVectors objects. Because this takes quite some time, we have also pickled these objects and stored those into the `/embeddings/compiled/` folder, so it's not necessary to compile these objects from scratch when running our code.

## Usage
After the requirements above are fulfilled, our notebook can be opened by running `jupyter notebook main.ipynb` in the terminal. The notebook should show previously computed results. The code can be run from scratch inside the notebook environment as well.

## Credits
This practical was made by [Rasyan Ahmed](https://github.com/rasyan) & [Marianne de Heer Kloots](https://github.com/mdhk).
