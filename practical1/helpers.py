import os
import pickle
import pandas as pd
from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.models import KeyedVectors
import random

def compile_models(embeddings_dir):
    if not os.path.exists(embeddings_dir + 'w2v/') or len(os.listdir(embeddings_dir + 'w2v/')) < 3:
        print('Converting models into word2vec format...')
        if not os.path.exists(embeddings_dir + 'w2v/'):
            os.makedirs(embeddings_dir + 'w2v/')
        files = [str for str in os.listdir(embeddings_dir) if str.endswith('.words')]
        for file in files:
            glove2word2vec(embeddings_dir + file, embeddings_dir + 'w2v/' + file)
        print('\tdone')
    else:
        print('Using word2vec-converted models.')

    print('Loading models...')
    print('\t> bow5')
    bow5_vectors = KeyedVectors.load_word2vec_format('embeddings/w2v/bow5.words')
    print('\t> bow2')
    bow2_vectors = KeyedVectors.load_word2vec_format('embeddings/w2v/bow2.words')
    print('\t> deps')
    deps_vectors = KeyedVectors.load_word2vec_format('embeddings/w2v/deps.words')
    print('\tdone')

    bow5_vectors.init_sims(replace=True)
    bow2_vectors.init_sims(replace=True)
    deps_vectors.init_sims(replace=True)

    if not os.path.exists(embeddings_dir + 'compiled/'):
        os.makedirs(embeddings_dir + 'compiled/')
    pickle.dump(bow5_vectors, open(embeddings_dir + 'compiled/bow5.pkl', 'wb'))
    pickle.dump(bow2_vectors, open(embeddings_dir + 'compiled/bow2.pkl', 'wb'))
    pickle.dump(deps_vectors, open(embeddings_dir + 'compiled/deps.pkl', 'wb'))
    print('Stored compiled models into', embeddings_dir + 'compiled/.')

def simlex_to_tsv(filepath):
    bow5_vectors=pickle.load(open('embeddings/compiled/bow5.pkl', 'rb'))
    bow2_vectors=pickle.load(open('embeddings/compiled/bow2.pkl', 'rb'))
    deps_vectors=pickle.load(open('embeddings/compiled/deps.pkl', 'rb'))
    simlex_data = pd.read_csv(filepath, sep='\t')
    similarities = {'bow5': [], 'bow2': [], 'deps': [], 'simlex': []}
    words = {'word1': [], 'word2': []}
    new_tsv = pd.DataFrame()

    for i in range(len(simlex_data)):
        word1, word2 = simlex_data['word1'][i], simlex_data['word2'][i]
        try:
            similarities['bow5'].append(bow5_vectors.similarity(word1, word2))
            similarities['bow2'].append(bow2_vectors.similarity(word1, word2))
            similarities['deps'].append(deps_vectors.similarity(word1, word2))
        except KeyError:
            continue
        words['word1'].append(word1)
        words['word2'].append(word2)
        simlex_score = simlex_data['SimLex999'][i]
        similarities['simlex'].append(simlex_score)

    new_tsv['word1'] = words['word1']
    new_tsv['word2'] = words['word2']
    new_tsv['SimLex-score'] = similarities['simlex']
    new_tsv.to_csv('datasets/SimLex-999/SimLex_scores.tsv', sep='\t', index=False)

def get_random_color(pastel_factor = 0.5):
    return [(x+pastel_factor)/(1.0+pastel_factor) for x in [random.uniform(0,1.0) for i in [1,2,3]]]

def color_distance(c1,c2):
    return sum([abs(x[0]-x[1]) for x in zip(c1,c2)])

#source = https://gist.github.com/adewes/5884820
def generate_new_color(existing_colors,pastel_factor = 0.5):
    max_distance = None
    best_color = None
    for i in range(0,100):
        color = get_random_color(pastel_factor = pastel_factor)
        if not existing_colors:
            return color
        best_distance = min([color_distance(color,c) for c in existing_colors])
        if not max_distance or best_distance > max_distance:
            max_distance = best_distance
            best_color = color
    return best_color

