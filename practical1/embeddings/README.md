# /embeddings/
If the models are to be compiled from the provided `.words` files (which can be obtained from [https://levyomer.wordpress.com/2014/04/25/dependency-based-word-embeddings/](https://levyomer.wordpress.com/2014/04/25/dependency-based-word-embeddings/)), those files should be put in this folder:

* `bow2.words`
* `bow5.words`
* `deps.words`

To save time we also store compiled (pickled) objects in the `compiled/` subfolder.
