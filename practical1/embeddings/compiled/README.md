# /embeddings/compiled/
In this folder we store the pickled objects for the BoW5, BoW2 and Deps embedding models, to save time:
* `bow2.pkl`
* `bow5.pkl`
* `deps.pkl`
