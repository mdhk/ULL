import re
from os import listdir
from gensim.models import KeyedVectors

def gold_to_dict(filepath):
    gold_list = [re.split('::|\;|, |\n', line) for line in open(filepath, 'r').readlines()]
    stripped_list = [[w.strip('.::1234567890; \n') for w in l if w != ''] for l in gold_list]
    gold_dict = {l[0] : l[1:] for l in stripped_list}
    return gold_dict

def import_embeddings(filepath):
    word_vectors = KeyedVectors.load_word2vec_format(filepath, binary=False, unicode_errors='ignore')
    vocab_list = word_vectors.vocab.keys()
    return list(vocab_list), word_vectors

def gold_sentences(filepath):
    sentence_list = [re.split('\t', line) for line in open(filepath, 'r').readlines()]
    sentences_dict = {s[-1].strip() : (s[0], s[1]) for s in sentence_list}
    return sentences_dict

def vocab_sub_dict(vocab_list, gold_dict):
    vocab_sub_dict = {key: [v for v in value if v in vocab_list]
                      for key, value in gold_dict.items() if key.split('.')[0] in vocab_list}
    return vocab_sub_dict

dir = 'embeddings/skipgram/'
for epoch, file in enumerate(listdir(dir)):
    filepath = dir + file
    try:
        vocab_list, word_embeddings = import_embeddings(filepath)
    except UnicodeDecodeError:
        continue
    gold_dict = gold_to_dict('lst/lst.gold.candidates')
    sub_dict = vocab_sub_dict(vocab_list, gold_dict)
    sentences_dict = gold_sentences('lst/lst_test.preprocessed')
    scored_sub_dict = {}

    for sentence, (target_word, sentence_id) in sentences_dict.items():
        if target_word not in sub_dict:
            continue
        sub_list = []
        target_stem = target_word.split(".")[0]
        for sub in sub_dict[target_word]:
            cosine_sim = word_embeddings.similarity(target_stem, sub)
            sub_list.append((sub, cosine_sim))
        scored_sub_dict[(target_word, sentence_id)] = sorted(sub_list, key=lambda x:x[1], reverse=True)

    out_file = open('lst/skipgram_lst_outs/skipgram_lst_ep' + str(epoch) + '.out', 'w')
    for (target_word, sentence_id), subs in scored_sub_dict.items():
        out_file.write('RANKED\t' + target_word + '\t' + sentence_id)
        for sub, score in subs:
            out_file.write('\t' + sub + ' ' + str(round(score, 4)) + '\t')
        out_file.write('\n')
    out_file.close()
