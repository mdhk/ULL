# Practical 2: Learning Word Representations

This repository contains the code for our second practical for the Unsupervised Language Learning course. In this practical, we tried to implement 3 word representation learning models: Skipgram, Bayesian Skipgram and EmbedAlign.

## Installation & Dependencies
After downloading or cloning our repository locally, the following dependencies should be satisfied before runing our code.

### Environment
Our code is written in a [Jupyter notebook](http://jupyter.org/install) which we ran using a Python 3.5 kernel.

### Libraries
We use the following non-default libraries, which should be installed before running our code:

* [gensim](https://radimrehurek.com/gensim/)
* [numpy](http://www.numpy.org/)
* [pytorch](https://pytorch.org/)

## Models
The code for our models are found in Jupyter notebooks with the corresponding names: `skipgram.ipynb`, `bayesian_skipgram.ipynb`, `embed_align.ipynb`. Previous results from our own runs of these models are shown when opening the notebooks, and the code can also be run from scratch. The notebook content can also be observed on Github directly.

## Evaluation
The LST evaluation for the Skipgram model can be run by executing `python run_lst_eval.py` in the main directory.

## Credits
This practical was made by [Rasyan Ahmed](https://github.com/rasyan) & [Marianne de Heer Kloots](https://github.com/mdhk).
