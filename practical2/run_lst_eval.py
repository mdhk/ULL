import os

dir = 'lst/skipgram_lst_outs/'
epochs = [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13]
for e, file in enumerate(os.listdir(dir)):
    filepath = dir + file
    epoch = epochs[e]
    print('== EPOCH', epoch, '==')
    os.system("python lst/lst_gap.py lst/lst_test.gold " + filepath + " out no-mwe")
